<?php get_header(); ?>

                <div class="map-museum-cont row">
                    <div class="col-md-12">
                        <iframe src="https://maps.osttirol.com/v2/" width="100%" height="300px" frameborder="0"> </iframe>
                    </div>
                </div>
                <?php
                    $museum_query_args = array(
                    	'post_type' => 'post',
                    	'posts_per_page' => 100,
                    	'ignore_sticky_posts' => true,
                    	'order' => 'ASC',
                    	'orderby' => 'date'
                    )
                ?>
                <?php $museum_query = new WP_Query( $museum_query_args ); ?>
                <?php if ( $museum_query->have_posts() ) : ?>
                    <div <?php post_class( 'feature-cont row' ); ?> id="post-<?php the_ID(); ?>">
                        <?php while ( $museum_query->have_posts() ) : $museum_query->the_post(); ?>
                            <?php PG_Helper::rememberShownPost(); ?>
                            <div class="col-md-6 museum-item
			">
                                <div class="col-md-6 mus-main float-left">
                                    <div class="row museum-title museum">
                                        <div>
                                            <a href="<?php echo esc_url( get_permalink( null, true ) ); ?>"><?php the_title(); ?></a>
                                        </div>
                                    </div>
                                    <?php if ( get_field( 'yx' ) ) : ?>
                                        <div class="row feature-location museum">
                                            <?php echo get_field( 'yx' ); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <a href=""></a>
                                <?php $image_attributes = !empty( get_the_ID() ) ? wp_get_attachment_image_src( PG_Image::isPostImage() ? get_the_ID() : get_post_thumbnail_id( get_the_ID() ), 'museum2' ) : null; ?>
                                <div class="col-md-6 feature-img-col float-lg-right museen" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
                                    <div class="col-md-12 feature-home-lg">
                                        <a href="#"> </a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'euregio2021' ); ?></p>
                <?php endif; ?>
                <div class="footer">
</div>                

<?php get_footer(); ?>