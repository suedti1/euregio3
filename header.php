<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="">
        <!-- Bootstrap core CSS -->
        <!-- Custom styles for this template -->
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body class="<?php echo implode(' ', get_body_class()); ?>">
        <?php if( function_exists( 'wp_body_open' ) ) wp_body_open(); ?>
        <div class="container">
            <div class="row">
                <div class=" header-dark ">
                    <div class="row pg-empty-placeholder"></div>
                    <div class="container">
                        <div class="row ">
                            <div class="col-md-4">
                                <img class="search-icon" src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/search.svg"/>
                                <?php get_search_form( true ); ?>
                            </div>
                            <div class="col-md-4">
</div>
                            <div class="col-md-4 lang-sel-cont">
                                <h3 class="lang-sel"><?php do_action('wpml_add_language_selector');  ?></h3>
                            </div>
                        </div>
                        <div class="row align-items-end">
                            <div class="col-md-6 pl-0 ml-0">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/logo.svg" class="main-logo-lg">
                            </div>
                            <div class="col-md-6 pl-0 ml-0">
                                <div class="menu-cont align-bottom">
                                    <?php wp_nav_menu( array(
                                        	'menu' => 'primary',
                                        	'menu_class' => 'menu-cont align-bottom',
                                        	'container' => '',
                                        	'fallback_cb' => 'wp_bootstrap4_navwalker::fallback',
                                        	'walker' => new wp_bootstrap4_navwalker()
                                    ) ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container feature-home">
            <div class="euregio-main">