
            </div>
            <div class="footer mt-5- row">
                <div class="col-md-5
			">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/euregio-footer-logo.svg" class="footer-logo-lg pb-2 pt-4"> 
                </div>
                <div class="col-md-1 offset-md-4 pt-4"> 
                    <a href="#"><?php _e( 'Impressum', 'euregio2021' ); ?></a>
                </div>
                <div class="col-md-1 pt-4"> 
                    <a href="#"><?php _e( 'Datenschutz', 'euregio2021' ); ?></a>
                </div>
            </div>
        </div>
        <!-- /.container -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <?php wp_footer(); ?>
    </body>
</html>
