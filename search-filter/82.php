<?php
/**
 * Search & Filter Pro
 *
 * Sample Results Template
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 *
 * Note: these templates are not full page templates, rather
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>



	<?php
	 	$post_query_item_number = 0;

	while ($query->have_posts())
	{
		$query->the_post();

		?>
		<?php if( $post_query_item_number >= 0 && $post_query_item_number <= 200 ) : ?>
				<?php PG_Helper::rememberShownPost(); ?>
				<div class="col-md-6 pl-0 pr-0 grid-item" id="post-<?php the_ID(); ?>">
						<?php  $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>

						<div class="list-img list-img-cont" style="<?php  echo 'background-image:url(\''.esc_url($featured_img_url).'\')' ?>"></div>
						<div class=" list-data mt-4">
								<div class="row">
										<div class="col-md-12">
												<?php the_terms( get_the_ID(), 'format', '<div class="format-tag">', '</div> <div class="format-tag">', '</div>' ); ?>
												<?php the_terms( get_the_ID(), 'topic', '<div class="topic-tag">', ' </div><div class="topic-tag">', '</div>' ); ?>
										</div>
								</div>
								<div class="row">
										<div class="col-md-12">
												<div class="list-title">
														<?php the_title(); ?>
												</div>
												<div class="list-date">
														<?php _e( 'Okt-Jun 2021', 'euregio2021' ); ?>
												</div>
												<div class="list-location">
														<?php _e( 'Naturmuseum Südtirol', 'euregio2021' ); ?>
												</div>
										</div>
								</div>
						</div>
				</div>
		<?php endif; ?>
		<?php $post_query_item_number++; ?>
		<?php
	}
	?>
	<?php
}
else
{
	echo "Keine Projekte mit diesem Filter ";
}
?>
