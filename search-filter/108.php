<?php
/**
 * Search & Filter Pro
 *
 * Sample Results Template
 *
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 *
 * Note: these templates are not full page templates, rather
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think
 * of it as a template part
 *
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs
 * and using template tags -
 *
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>



	<?php
	 	$post_query_item_number = 0;

	while ($query->have_posts())
	{
		$query->the_post();

		?>
		<?php if( $post_query_item_number >= 0 && $post_query_item_number <= 200 ) : ?>
		
				
				
				
				
				                          <?php PG_Helper::rememberShownPost(); ?>
                            <div <?php post_class( 'row proj-item' ); ?> id="post-<?php the_ID(); ?>">
	                            						<?php  $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>

                            
                                <div class="col-md-6 feature-img-col projekte" style="<?php  echo 'background-image:url(\''.esc_url($featured_img_url).'\')' ?>">
                                    <div class="col-md-12 feature-home-lg">
                                        <a href="#"> </a>
                                    </div>
                                </div>
                                <div class="col-md-6 feature-main pb-4">
                                    <div class="row feature-tags">
                                        <div class="col-md-12">
                                            <?php the_terms( get_the_ID(), 'format', '<div class="format-tag">', '</div> <div class="format-tag">', '</div>' ); ?>
                                            <?php the_terms( get_the_ID(), 'topic', '<div class="topic-tag">', ' </div><div class="topic-tag">', '</div>' ); ?>
                                        </div>
                                    </div>
                                    <div class="row feature-title ml-1 ">
                                        <a href="<?php echo esc_url( the_permalink() ); ?>"><?php the_title(); ?></a>
                                    </div>
                                    <div class="row feature-subtitle">
                                        <div>
                                            <?php the_excerpt( ); ?>
                                        </div>
                                    </div>
                                    <?php if ( get_field( 'von' ) ) : ?>
                                        <div class="row feature-date ml-1 ">
                                            <?php echo get_field( 'von' ); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ( get_field( 'yx' ) ) : ?>
                                        <div class="row feature-location ml-1 ">
                                            <?php echo get_field( 'yx' ); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
				
				
				
				
		<?php endif; ?>
		<?php $post_query_item_number++; ?>
		<?php
	}
	?>
	<?php
}
else
{
	echo "Keine Projekte mit diesem Filter ";
}
?>
