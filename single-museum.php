<?php get_header(); ?>

                <div class="row">
                    <div class="col-md-12">
                        <h3 class="single-title"><?php echo get_field( 'slogan' ); ?></h3>
                    </div>
                </div>
                <div class="row single-divider">
                    <div class="col-md-12">
                        <div class="single-back">
                            <span>&lt;</span>
                            <?php _e( 'Zurück zur Projektliste', 'euregio2021' ); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="single-content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?php get_template_part( 'parts/gallery' ); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 grey-divider">
                        <p class="grey-divider-text pl-0"><?php _e('Ein Blick hinter die Kulissen', 'euregio2021') ?></p>
                    </div>
                </div>
                <?php get_template_part( 'parts/related', 'proj.php' ); ?>
                <div class="row">
                    <div class="col-md-12 grey-divider">
                        <p class="grey-divider-text"><?php _e( 'Öffnungszeiten und Besucherinfos', 'euregio2021' ); ?></p>
                    </div>
                </div>
                <div class="map-museum-cont row">
                    <div class="col-md-3 single-address">
                        <h3 class="single-subtitles"><?php _e( 'WEBSEITE', 'euregio2021' ); ?></h3>
                        <p class="single-data"><?php _e( 'www.xxx.com', 'euregio2021' ); ?></p>
                        <h3 class="single-subtitles"><?php _e( 'ADRESSE', 'euregio2021' ); ?></h3>
                        <p class="single-data"> <?php _e( 'Schloss Landeck', 'euregio2021' ); ?></p>
                        <h3 class="single-subtitles"><?php _e( 'ÖFFNUNGSZEITEN', 'euregio2021' ); ?></h3>
                        <p class="single-data"><?php _e( 'Mo bis Fr: 9:30–17 Uhr', 'euregio2021' ); ?><br><?php _e( 'Sa und So: 12–19 Uhr', 'euregio2021' ); ?></p>
                    </div>
                    <div class="col-md-9 ml-0 pr-0">
                        <iframe src="https://maps.osttirol.com/v2/" width="100%" height="400px" frameborder="0"> </iframe>
                    </div>
                </div>
                <div class="footer">
</div>                

<?php get_footer(); ?>