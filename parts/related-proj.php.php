<div class="pg-empty-placeholder related-projects row">


<?php
  // If this file is called directly, abort.
  if ( ! defined( 'ABSPATH' ) ) {
  	exit;
  }


  $featured_posts = get_field('featured_posts');
if( $featured_posts ): ?>


  	<?php

      foreach( $featured_posts as $post ):

          // Setup this post for WP functions (variable must be named $post).
          setup_postdata($post);

  		?>
  				<div class="col-md-6 pl-0 pr-0 grid-item item-related" id="post-<?php the_ID(); ?>">
  						<?php  $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>

  						<div class="list-img list-img-cont" style="<?php  echo 'background-image:url(\''.esc_url($featured_img_url).'\')' ?>"></div>
  						<div class=" list-data mt-4">
  								<div class="row">
  										<div class="col-md-12">
  												<?php the_terms( get_the_ID(), 'format', '<div class="format-tag">', '</div> <div class="format-tag">', '</div>' ); ?>
  												<?php the_terms( get_the_ID(), 'topic', '<div class="topic-tag">', ' </div><div class="topic-tag">', '</div>' ); ?>
  										</div>
  								</div>
  								<div class="row">
  										<div class="col-md-12">
  												<div class="list-title">
  														<?php the_title(); ?>
  												</div>
  												<div class="list-date">
  														<?php _e( 'Okt-Jun 2021', 'euregio2021' ); ?>
  												</div>
  												<div class="list-location">
  														<?php _e( 'Naturmuseum Südtirol', 'euregio2021' ); ?>
  												</div>
  										</div>
  								</div>
  						</div>
  				</div>
    <?php endforeach; ?>


    		<?php endif; ?>



  <?php
  // Reset the global post object so that the rest of the page works correctly.
  wp_reset_postdata(); ?>



</div>
