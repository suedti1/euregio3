<div class="flex-content">
  <?php
	if( have_rows('blocks') ): ?>
	<?php while( have_rows('blocks') ): the_row(); ?>

	<!-- Layout general_paragraph -->
	<?php if(get_row_layout() === 'text' ):

	unset($images);
	unset($posts_rel);
	$images = get_sub_field('gallery');
//	$posts_rel = get_sub_field('general_rel');
	$sub_title=get_sub_field('titel');
	?>

	<?php $text=get_sub_field('general_text');
		$t=get_sub_field('text');


						if ($sub_title!=''){
				echo	'<div class="row ">
						<div class="col-sm-10 col-xs-12 colCenter text-left Corpres">

					<h1>'.$sub_title.'</h1>
							</div>

					</div>';
			}
		 ?>



	<div class="row general_text">



				<div class="col-md-8 col-sm-8 col-xs-12 Corpres">
<?php
			if( $images ): ?>


				<?php $i=0;


				 $image=$images[0];
							$img = wp_get_attachment_image_src($image['ID'], 'gallery');
							$img_big=wp_get_attachment_image_src($image['ID'], 'fullscreen');
							$img_sm=wp_get_attachment_image_src($image['ID'], 'gallery-thumb');
							?>
							<a class="rsImg" data-rsBigImg="<?php echo $img_big[0]; ?>" href="<?php echo $img_big[0]; ?>" alt="<?php echo  $image['alt']; ?>"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" ></a><?php if ($image['caption']!=''){echo '<div class="caption"> '.$image['caption'].'</div>';} ?>


					<?php
					if ((!($posts_rel))&&($text='')){?>
					</div><!-- end images single //close row only if there is no text or related sidebar -->
					<?php }	?>

	 <?php endif; //images

		 echo '<!-- end images  -->';
	 ?>

	<?php  if( $t!='' ):

	?>
					<div class="txt Corpres">
						<?php echo $t; ?>
					</div> <!-- end col  text-->
	<?php endif;?>




	//	<?php
	//		if (!($posts_rel)) {  //close row only if the are no images and no related column
	//
				echo '</div> <!-- end row text without sidebar-->';
//			}



    //
		// 	// RELATED SIDEBAR
		// 	?>
    //
		// <?php if( ($posts_rel) ): ?>
		// <div class="col-md-4 col-sm-4 col-xs-12 Corpres">
    //
		// 	<div class="sidebar-blocks">
		// 	<?php if(!empty( $posts_rel)){?>
		// 		<?php  foreach( $posts_rel as $p ):
		// 		// variable must NOT be called $post (IMPORTANT)
		// 		if ((get_field('general_background', $p->ID)==1)){
		// 			//wenn der Block mit hintergrundbild ausgegeben werden soll
		// 			$bg_style='style="background:url('.wp_get_attachment_image_src(get_post_thumbnail_id($p->ID), "block-bg")[0].');" ';
		// 			}
		// 			else{$bg_style='';}
		// 			echo  ' <div class="posts-block" '.$bg_style.'>'; ?>
		// 		<div class="sidebar-format">
		// 			<?php echo get_the_term_list( $p->ID, 'format', '', '  ' ); ?>
		// 		</div>
		// 		<div class="sidebar-title">
		// 			<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a>
		// 		</div>
		// 		<div class="sidebar-content">
		// 					<?php
		// 				if ((get_field('exzerpt_sidebox', $p->ID)!='')){
		// 					the_field('exzerpt_sidebox', $p->ID);
		// 				}
		// 				else{
		// 				echo  wp_trim_words($p->post_content, 10, '...' );
		// 				}
		// 						edit_post_link('edit', '<br>', '',$p->ID);
    //
		// 				?>
    //
		// 		</div>
		// 		<a class="link_icon" href="<?php echo get_permalink( $p->ID ); ?>"></a>
		// 	</div>
		// 	<?php endforeach; ?>
		// 		</div> <!-- Close col  -->
		// 		</div> <!-- Close row  -->
		// 		</div> <!-- Close row  -->
    //
		// 	<?php }?>
    //
    //
    //
    //
		// 	<?php endif;?>






		<?php elseif( get_row_layout() === 'general_youtube' ):?>

		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12 Corpres">
				<?php
				echo '<a name="'.urlencode(get_sub_field('video_title')).'"></a>';
				echo '<strong>'.get_sub_field('video_title').'</strong><br>';?>
				<div class="yt-cont">
					<iframe width="853" height="505" src="https://www.youtube.com/embed/<?php echo get_sub_field('youtube_url');?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="video-subtitle"><?php echo get_sub_field('video-subtitle');?></div>
			</div>
		</div>



		<?php endif; //layouts ?>

	<?php  endwhile; ?>

	<?php endif; ?>



</div>
