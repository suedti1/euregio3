<div class="single-media">




	<?php
	$images = get_field('gallery');
	if( $images ): ?>
	    <div id="slider" class="flexslider">
	        <ul class="slides">
	            <?php foreach( $images as $image ): ?>
	                <li>
	                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
	                    
											<p class="flex-caption"><?php echo esc_html($image['caption']); ?></p>
	                </li>
	            <?php endforeach; ?>
	        </ul>
	    </div>

	<?php endif; ?>


</div>
