<?php get_header(); ?>

<?php
    $feat_query_args = array(
    	'post__in' => get_option( 'sticky_posts' ),
    	'post_type' => 'post',
    	'posts_per_page' => 1,
    	'ignore_sticky_posts' => true,
    	'order' => 'ASC',
    	'orderby' => 'date'
    )
?>
<?php $feat_query = new WP_Query( $feat_query_args ); ?>
<?php if ( $feat_query->have_posts() ) : ?>
    <div class="feature-cont">
        <?php while ( $feat_query->have_posts() ) : $feat_query->the_post(); ?>
            <?php PG_Helper::rememberShownPost(); ?>
            <div <?php post_class( 'row' ); ?> id="post-<?php the_ID(); ?>">
                <?php $image_attributes = !empty( get_the_ID() ) ? wp_get_attachment_image_src( PG_Image::isPostImage() ? get_the_ID() : get_post_thumbnail_id( get_the_ID() ), 'medium_large' ) : null; ?>
                <div class="col-md-6 feature-img-col" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
                    <div class="col-md-12 feature-home-lg">
                        <a href="#"> </a>
                    </div>
                </div>
                <div class="col-md-6 feature-main pb-4">
                    <div class="row feature-tags">
                        <div class="col-md-12">
                            <?php the_terms( $post->ID, 'format', '<div class="format-tag">', '</div> <div class="format-tag">', '</div>' ); ?>
                            <?php the_terms( $post->ID, 'topic', '<div class="topic-tag">', ' </div><div class="topic-tag">', '</div>' ); ?>
                        </div>
                    </div>
                    <div class="row feature-title ml-1 ">
                        <a href="<?php echo esc_url( the_permalink() ); ?>"><?php the_title(); ?></a>
                    </div>
                    <div class="row feature-subtitle">
                        <div>
                            <?php the_excerpt( ); ?>
                        </div>
                    </div>
                    <?php if ( get_field( 'von' ) ) : ?>
                        <div class="row feature-date ml-1 ">
                            <?php echo get_field( 'von' ); ?>
                        </div>
                    <?php endif; ?>
                    <?php if ( get_field( 'yx' ) ) : ?>
                        <div class="row feature-location ml-1 ">
                            <?php echo get_field( 'yx' ); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
    </div>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'euregio2021' ); ?></p>
<?php endif; ?>
<div class="row">
    <div class="bg-secondary col-md-12 filter filter-bar">
        <?php do_shortcode( '[searchandfilter id=1 action=filter_next_query]
        ') ?>
    </div>
</div>
<?php
    $post_query_args = array(
    	'post_type' => 'post',
    	'posts_per_page' => 200,
    	'paged' => get_query_var( 'paged' ) ?: 1,
    	'ignore_sticky_posts' => true,
    	'order' => 'ASC',
    	'orderby' => 'date'
    )
?>
<?php $post_query = new WP_Query( $post_query_args ); ?>
<?php if ( $post_query->have_posts() ) : ?>
    <div class="row main-grid">
        <?php $post_query_item_number = 0; ?>
        <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
            <?php if( $post_query_item_number >= 0 && $post_query_item_number <= 200 ) : ?>
                <?php PG_Helper::rememberShownPost(); ?>
                <div class="col-md-6 pl-0 pr-0 grid-item<?php if( $post_query_item_number == 0) echo ' first'; ?> <?php echo join( ' ', get_post_class( '' ) ) ?>" id="post-<?php the_ID(); ?>">
                    <?php $image_attributes = !empty( get_the_ID() ) ? wp_get_attachment_image_src( PG_Image::isPostImage() ? get_the_ID() : get_post_thumbnail_id( get_the_ID() ), 'medium' ) : null; ?>
                    <div class="list-img list-img-cont" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>"></div>
                    <div class=" list-data mt-4">
                        <div class="row">
                            <div class="col-md-12"> 
                                <?php the_terms( $post->ID, 'format', '<div class="format-tag">', '</div> <div class="format-tag">', '</div>' ); ?>
                                <?php the_terms( $post->ID, 'topic', '<div class="topic-tag">', ' </div><div class="topic-tag">', '</div>' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="list-title">
                                    <?php the_title(); ?>
                                </div>
                                <div class="list-date">
                                    <?php _e( 'Okt-Jun 2021', 'euregio2021' ); ?>
                                </div>
                                <div class="list-location">
                                    <?php _e( 'Naturmuseum Südtirol', 'euregio2021' ); ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php $post_query_item_number++; ?>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
    </div>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'euregio2021' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>